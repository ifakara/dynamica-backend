<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <title>Dynamica | Form Builder</title>
    <link rel="stylesheet" href="../bower_components/angular/angular-csp.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/textAngular/dist/textAngular.css">
    <link rel="stylesheet" href="../dist/form-builder-bootstrap.min.css">
    <link rel="stylesheet" href="../dist/form-viewer.min.css">
    <link rel="stylesheet" href="demo.css">
</head>

<body ng-controller="DemoController as ctrl" ng-cloak>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid" style="border-bottom: 1px solid #e7e7e7;" role="presentation">
        <div class="navbar-header" role="presentation" aria-hidden="true">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span><b>Dynamica</b> | Form builder </span></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse" role="presentation">
            <ul class="nav navbar-nav nav-pills navbar-right" role="menubar">
                
            </ul>
        </div>
    </div>
</nav>


<div class="demo-container" ng-if="ctrl.formData">

    <uib-tabset active="active">

        <uib-tab select="ctrl.setSelectedTabIndex(0)" index="0" heading="Builder">
            <div class="row" ng-if="ctrl.selectedTabIndex===0">
                <div class="col-md-offset-2 col-md-8">
                    <div class="form-inline">

                        <div class="row">
                            <div class="col-xs-2 col-md-4 col-lg-2 col-xs-pull-0">

                                <label for="sel1">Form access level</label>
                                <select class="form-control" id="sel1">

                                <?php

                                ini_set('display_errors', 'off');
                                require_once('../Backend.php');

                                $gid = null;

                                if (isset($_GET['id'])) {
                                    $id = $_GET['id'];
                                    $array = Backend::getInstance()->getFormAttributes($id);

                                    $res = json_encode($array);

                                    foreach ($array as $key => $value) {
                                        $gid = $value['groupId'];
                                    }


                                }

                                $array = Backend::getInstance()->getRoles();

                                $res = json_encode($array);

                                foreach ($array as $key => $value){
                                    $id = $value['id'];
                                    $val = $value['group'];

                                    if ($id == $gid) {
                                        echo "<option value=\"$id\" selected>$val</option>";
                                    } else {
                                        echo "<option value=\"$id\">$val</option>";
                                    }
                                }

                                echo " </select>";

                                ?>

                            </div>

                            <div class="col-xs-1 col-md-3 col-lg-1 col-xs-pull-0">

                                <label for="sel2">Study</label>
                                <select class="form-control" id="sel2">
                                    <?php
                                    ini_set('display_errors', 'off');
                                    require_once('../Backend.php');
                                    $sid = null;

                                    if (isset($_GET['id'])) {
                                        $id = $_GET['id'];

                                        $array = Backend::getInstance()->getFormAttributes($id);

                                        $res = json_encode($array);

                                        foreach ($array as $key => $value) {
                                            $sid = $value['studyId'];
                                        }
                                    }

                                    $array = Backend::getInstance()->getStudies();

                                    $res = json_encode($array);

                                    foreach ($array as $key => $value) {
                                        $id = $value['id'];
                                        $val = $value['group'];
                                        if ($id == $sid) {
                                            echo "<option value=\"$id\" selected>$val</option>";
                                        } else {
                                            echo "<option value=\"$id\">$val</option>";
                                        }
                                    }

                                    echo "</select>";

                                    ?>


                            </div>


                        </div>
                        <p></p>

                        <div class="row">
                            <h6>&nbsp;</h6>
                        </div>

                    </div>


                    <mw-form-builder api="ctrl.formBuilder" options="ctrl.optionsBuilder" form-data="ctrl.formData"
                                     form-status="ctrl.formStatus" read-only="ctrl.builderReadOnly"
                                     on-image-selection="ctrl.onImageSelection()"></mw-form-builder>
                </div>
            </div>
        </uib-tab>

        <uib-tab select="ctrl.setSelectedTabIndex(1)" heading="Preview Form">

            <div ng-if="ctrl.selectedTabIndex===1">

                <mw-form-viewer form-data="ctrl.formData" template-data="ctrl.templateData"
                                form-status="ctrl.formStatus" options="ctrl.formOptions" api="ctrl.formViewer"
                                response-data="ctrl.responseData" read-only="ctrl.viewerReadOnly"
                                on-submit="ctrl.saveResponse()"></mw-form-viewer>

                <hr>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="ctrl.showResponseData"> Show response data
                    </label>
                </div>
                        <pre ng-if="ctrl.showResponseData">
{{ctrl.responseData|json}}
                        </pre>
            </div>

        </uib-tab>

        <uib-tab heading="Show JSON">
                    <pre>
                        {{ctrl.formData|json}}
                    </pre>
        </uib-tab>

    </uib-tabset>

    <div class="row">

        <div class="text-center btn-group-md" style="margin-top: 15px;">
            <button type="button" style="margin-bottom: 15px" class="btn btn-success"
                    ng-click="ctrl.submitForm()">Publish form
            </button>

            <button type="button" style="margin-bottom: 15px" class="btn btn-danger"
                    ng-click="ctrl.resetBuilder()">Reset form
            </button>
        </div>


    </div>

    <!-- The actual snackbar -->
    <div id="snackbar">Form published!</div>

</div>


<script src="../bower_components/angular/angular.js"></script>
<script src="../bower_components/textAngular/dist/textAngular-sanitize.js"></script>
<script src="../bower_components/angular-translate/angular-translate.min.js"></script>
<script
    src="../bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
<script src="../bower_components/angular-elastic/elastic.js"></script>
<script src="../bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
<script src="../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="../bower_components/Sortable/Sortable.min.js"></script>
<script src="../vendor/angular-legacy-sortable.js"></script>
<script src="../bower_components/textAngular/dist/textAngular-rangy.min.js"></script>
<script src="../bower_components/textAngular/dist/textAngular.min.js"></script>
<script src="../dist/form-utils.js"></script>
<script src="../dist/form-builder.js"></script>
<script src="../dist/form-builder-bootstrap-tpls.min.js"></script>
<script src="../dist/form-viewer.js"></script>
<script src="../dist/form-viewer-bootstrap-tpls.min.js"></script>
<script src="builder.js"></script>

<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    ini_set('display_errors', 'off');
    require_once('../Backend.php');

    $data = Backend::getInstance()->getFormById($id);

    if ($data != null) {

        file_put_contents('form-data.json', $data);

        $array = Backend::getInstance()->getFormAttributes($id);

        $res = json_encode($array);

        foreach ($array as $key => $value) {
            $gid = $value['groupId'] - 1;
            $sid = $value['studyId'] - 1;
        }

    } else {
        file_put_contents('form-data.json', "{}");
    }

} else {
    file_put_contents('form-data.json', "{}");
}
?>

</body>
</html>