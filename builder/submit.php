<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 6/4/2017
 * Time: 7:49 PM
 */

ini_set('display_errors', 'off');

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    header('Content-Type: application/json');

    require_once('../Backend.php');

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata,true);

    $accessGroup = $request['group'];
    $formSchema = json_encode($request['jsonStuff'],JSON_PRETTY_PRINT);
    $formName = $request['formName'];
    $formDesc = $request['formDesc'];
    $formStudy = $request['formStudy'];
    $id = $request['formId'];
    
    if($id != null){
        Backend::getInstance()->updateForm($id, $formName, $formDesc, $formStudy, $formSchema, $accessGroup);
    } else{
        Backend::getInstance()->saveForm($formName, $formDesc, $formStudy, $formSchema, $accessGroup);
    }



}


