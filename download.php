<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 6/6/2017
 * Time: 10:18 AM
 */

$fileName = basename('dynamica-v1b.apk');
$filePath = 'files/'.$fileName;
if(!empty($fileName) && file_exists($filePath)){
    // Define headers
    $length = filesize($filePath);
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Type: application/*");
    header("Content-Length: $length");
    header("Content-Transfer-Encoding: binary");

    // Read the file
    readfile($filePath);
    exit;
}else{
    echo 'The file does not exist.';
}