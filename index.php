<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dynamica Frontpage</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

<nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header"><a href="#" class="navbar-brand navbar-link"><strong>Dynamica</strong> | Manage forms</a>
                <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <!--<ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="builder"><b>Create new form</b></a></li>
                </ul>-->
            </div>
        </div>
    </nav>
    <div class="container"> <div class="row"><h4>Published forms:</h4></div></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="builder">
                    <button class="btn btn-success" type="button">Create a <strong>new form</strong></button>
                </a>
            </div>
        </div>
        <div >
            <?php
            ini_set('display_errors', 'off');
            require_once('Backend.php');

            $array = Backend::getInstance()->getForms();

            foreach ($array as $key => $value){
                $id = $value['formId'];
                $name = $value['formName'];
                $desc = $value['formDesc'];
                $mod = $value['modified'];
                $rev = $value['formRev'];


                $index = $key + 1;

                echo "<a href='builder/index.php?id=$id'><div id=\"form-list-item\" class=\"row\"><div class=\"col-xs-12\">
                <div class=\"col-xs-1\">
                    <h4 class=\"text-right text-muted\">$index</h4></div>
                <div class=\"col-xs-6\">
                    <h4 id=\"form-name\">$name</h4>
                    <h5 class=\"text-muted\" id=\"form-desc\">$desc</h5></div>
                <div class=\"col-xs-3\">
                    <h5 class=\"text-left text-muted\">Date modified</h5>
                    <h5 id=\"date-modified\">$mod</h5></div>
                <div class=\"col-xs-2\">
                <h5 class=\"text-left text-muted\">Form revision</h5>
                <h5 id=\"date-modified\">$rev</h5></div>
                </div></div></a>";
            }

            ?>
        </div>

    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>