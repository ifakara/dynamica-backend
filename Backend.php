<?php

/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 5/2/2017
 * Time: 11:27 AM
 */
class Backend {

    private static $instance;

    private function __construct() {
    }

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Backend();
        }

        return self::$instance;
    }


    public function getStudies() {
        $con = self::getConnection();

        $sql = $con->prepare("SELECT id,study FROM form_study");

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($id, $study);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array(
                    'id' => $id,
                    'group' => $study
                ));
            }

            return $results;

        } else {

            return array();
        }
    }

    /** START STUDY Management*/

    // Create STUDY

    // Get STUDY
    private function getConnection() {
        $host = 'localhost';
        $user = 'wickerman';
        $pass = '';
        $dbname = 'dynamica';

        /*$host = 'localhost';
        $user = 'wickigcb_backend';
        $pass = 'z=%c=q(;Wt!i]BgneD';
        $dbname = 'wickigcb_dynamica';*/

        $con = mysqli_connect($host, $user, $pass, $dbname) or die(json_encode(array(
            'Error' => 'Failed to connect to database'
        )));

        return $con;
    }

    // Update STUDY

    // Delete STUDY

    /** END STUDY Management*/


    /** START ROLES Management */

    // Create ROLE


    // Read ROLES

    public function getRoles() {
        $con = self::getConnection();

        $sql = $con->prepare("SELECT id,usergroup FROM access_groups");

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($id, $userGroup);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array(
                    'id' => $id,
                    'group' => $userGroup
                ));
            }

            return $results;

        } else {

            return array();
        }
    }

    //Update ROLE

    //Delete ROLE

    /** END ROLES Management */


    /** START FORM Management */

    // Create FROM
    public function saveForm($name, $desc, $studyId, $schema, $group) {

        $con = $this->getConnection();

        $con->autocommit(false);

        $sql = $con->prepare("INSERT INTO dyna_forms(form_name,description,json_schema,user_group,study_id) VALUES (?,?,?,?,?)");
        $sql->bind_param('sssii', $name, $desc, $schema, $group, $studyId);

        if ($sql->execute()) {
            $con->commit();
        } else {
            echo $sql->error;
            $con->rollback();
        }
    }

    // Get FORM
    public function getForms() {

        $con = $this->getConnection();

        $sql = $con->prepare("SELECT id,form_name,description,user_group,json_schema,revision,modified FROM dyna_forms ");

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($id, $name, $desc, $group, $schema, $rev, $modified);

            $results = array();
            while ($sql->fetch()) {
                array_push($results, array(
                    'formId' => $id,
                    'formName' => $name,
                    'formDesc' => $desc,
                    'accessGroup' => $group,
                    'jsonSchema' => $schema,
                    'formRev' => $rev,
                    'modified' => $modified
                ));
            }

            return $results;

        } else {

            return array();
        }
    }

    public function getFormAttributes($id) {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT user_group,study_id FROM dyna_forms WHERE id = ?");
        $sql->bind_param('i', $id);

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($gid, $sid);

            $sql->fetch();

            $results = array();

            array_push($results, array(
                'groupId' => $gid,
                'studyId' => $sid
            ));


            return $results;

        } else {

            return '';
        }
    }

    public function getFormById($id) {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT json_schema FROM dyna_forms WHERE id = ?");
        $sql->bind_param('i', $id);

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($schema);

            $sql->fetch();

            return $schema;

        } else {

            return '';
        }
    }

    // Update FORM
    public function updateForm($id, $name, $desc, $studyId, $schema, $group) {

        $con = $this->getConnection();

        $con->autocommit(false);

        $sql = $con->prepare("UPDATE dyna_forms SET form_name = ?, description = ?, json_schema = ?, user_group = ?, study_id = ?, revision = revision + 1 WHERE id = ?");
        $sql->bind_param('sssiii', $name, $desc, $schema, $group, $studyId, $id);

        if ($sql->execute()) {
            $con->commit();
        } else {
            echo $sql->error;
            $con->rollback();
        }
    }

    // Delete FORM


    /** END FORM Management */


    /** START USER Management **/

    // Create user
    public function createUser($username, $password, $study_id) {
        $con = $this->getConnection();

        $con->autocommit(false);

        $sql = $con->prepare("INSERT INTO users(username, password, study_id) VALUES (?,?,?)");
        $sql->bind_param('ssi', $username, $password, $study_id);

        if ($sql->execute()) {
            $con->commit();
            return true;
        } else {
            $con->rollback();
            return false;
        }

    }

    //Update user password
    public function updatePassword($uid, $newPassword) {
        $con = $this->getConnection();

        $con->autocommit(false);


    }


    /** END USER Management */



}