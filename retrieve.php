<?php
/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 6/5/2017
 * Time: 1:23 PM
 */

require_once ('Backend.php');

header('Content-Type: application/json');

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    if(isset($_POST['client']) && $_POST['client'] == 'mobile'){

        echo json_encode(Backend::getInstance()->getForms(), JSON_PRETTY_PRINT);

    } else {

        echo json_encode(array(
            'error'=>'You did some invalid stuff'
        ));

    }

}